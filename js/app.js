let billAmount = 0;
let tipVal = .00;
const range = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

$(".lowerSection").on('click', function(e) {
	let clickTarget = e.target.innerHTML;
	let clickVal = Number(clickTarget);

	// //check if the click target is a number
	if(range.indexOf(clickVal) > -1) {
		billAmount = Number(billAmount + "" + clickVal);
	}

 	//clear button - set billAmmount to 0 clear the #dataBill
 	if(clickTarget === "CLEAR") {
 		billAmount = 0;	
 	}

 	update();
});


$("#tip").on('click', function(e) {
	let clickTarget = e.target.innerHTML;
	let removeSymbol = clickTarget.slice(0, clickTarget.length - 1);
	let clickVal = Number(removeSymbol);

	if(clickVal <= 0) {
		tipVal = .05;
		$('#tip').text("5%");
	} else if (clickVal <= 5) {
		tipVal = .10;
		$('#tip').text("10%");		
	} else if (clickVal <= 10) {
		tipVal = .15;
		$('#tip').text("15%");		
	} else if (clickVal <= 15) {
		tipVal = .20;
		$('#tip').text("20%");		
	} else if (clickVal <= 20) {
		tipVal = .25;
		$('#tip').text("25%");		
	} else if (clickVal <= 25) {
		tipVal = .30;
		$('#tip').text("30%");		
	} else if (clickVal <= 30) {
		tipVal = .00;
		$('#tip').text("0%");		
	};

	//console.log(typeof clickVal);

	//call update here
	update();
});


function update() {
	//convert local billAmount to decimal places

	//number of digits in the interger
	//console.log(billAmount.toString().length);
	let strVersion = billAmount.toString();
	//console.log(typeof strVersion);
	//convert to string and insert .
	let insVersion = strVersion.slice(0, strVersion.length - 2) + "." + strVersion.slice(strVersion.length - 2, strVersion.length);
	//console.log(insVersion);
	//convert back to number
	let billVersion = Number(insVersion);
	//console.log(typeof billVersion);

	//update amountS each cycle through
 	$("#dataBill").text("$" + billVersion.toFixed(2));
 	$("#dataCost").text("$" + (billVersion * tipVal + billVersion).toFixed(2));
 	$("#dataTip").text("$" + (billVersion * tipVal).toFixed(2));
};